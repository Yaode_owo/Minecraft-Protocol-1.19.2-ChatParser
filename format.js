module.exports = format

function format (string, _with = []) {
  try {
  let i = 0

  return string.replace(/%(?:(\d+)\$)?(s|%)/g, (g0, g1) => {
    if (g0 === '%%') {
      return '%'
    } else {
      const idx = g1 ? parseInt(g1) - 1 : i++
      if (_with[idx] !== undefined && _with[idx].length !== undefined) {
		if (_with[idx].length > 5000) return;
	  };
      if (_with[idx] !== undefined) {
        return _with[idx]
      } else {
        return ''
      }
    }
  })
  } catch (error) { console.log(error); return; };
}