const ChatMessage = require('../util/ChatMessage')('1.19.2');
bot.on('systemChat', (data) => {
  const msg = ChatMessage.fromNotch(data.formattedMessage);
  const chatPositions = {
    1: 'system',
    2: 'game_info',
  };
  bot.emit('cmessage', msg, chatPositions[data.positionId], null);
  bot.emit('cmessagestr', msg.toString(), chatPositions[data.positionId], msg, null);
  if (data.positionId === 2) bot.emit('cactionBar', msg, null);
});